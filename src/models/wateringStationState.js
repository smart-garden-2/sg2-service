const wateringStationState = [
    "idle", // 0
    "watering_manual", // 1
    "watering_automatic", // 2
    "monitor", // 3
    "alarm" // 4
];

const defaultState = "monitor";

const parseState = (state) => {
    return wateringStationState[state] || undefined;
};

module.exports = {
    parseState,
    defaultState
};
