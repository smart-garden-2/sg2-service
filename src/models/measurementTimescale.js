const { fluxDuration } = require("@influxdata/influxdb-client");

const measurementTimescale = {
    day: fluxDuration("-24h"),
    week: fluxDuration("-7d"),
    month: fluxDuration("-30d"),
    year: fluxDuration("-360d")
};

const windowForTimescale = {
    day: fluxDuration("1m"),
    week: fluxDuration("1h"),
    month: fluxDuration("1d"),
    year: fluxDuration("1mo")
};

const timescales = ["day", "week", "month", "year"];
const defaultTimeRange = fluxDuration("-7d");

const parseTimescale = (period) => {
    return measurementTimescale[period] ?? undefined;
};

const getWindowFromPeriod = (period) => {
    return windowForTimescale[period] ?? undefined;
};

module.exports = {
    parseTimescale,
    defaultTimeRange,
    timescales,
    getWindowFromPeriod
};
