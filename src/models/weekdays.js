const weekdays = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday"
];

const getWeekdays = () => {
    return weekdays;
};

const getDay = (day) => {
    return weekdays.indexOf(day);
};

module.exports = { getWeekdays, getDay };
