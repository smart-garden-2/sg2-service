const measurements = [
    "temperature",
    "pressure",
    "tankLevel",
    "airHumidity",
    "soilHumidity"
];

module.exports = {
    measurements
};
