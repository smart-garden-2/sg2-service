const { valueNames } = require("../settings");
module.exports = function (mongoose) {
    const Schema = mongoose.Schema;
    const SettingSchema = new Schema({
        name: {
            type: String,
            required: true,
            enum: valueNames
        },
        value: {
            type: String,
            required: false,
            default: function () {
                if (this.name === "waterFlow") return "HALF";
                if (this.name === "soilHumidityThreshold") return "555";
                if (this.name === "dataGatheringFreq") return "each_second";
            }
        }
    });
    return mongoose.model("Setting", SettingSchema);
};
