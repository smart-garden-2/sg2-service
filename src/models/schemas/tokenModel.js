module.exports = function (mongoose) {
    const Schema = mongoose.Schema;
    const TokenSchema = new Schema({
        userToken: { type: String, required: true },
        authLevel: {
            type: String,
            required: false,
            enum: ["admin", "user"],
            default: "user"
        },
        userEmail: { type: String, required: true }
    });
    return mongoose.model("Token", TokenSchema);
};
