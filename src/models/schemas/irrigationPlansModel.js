const mongoose = require("mongoose");
const { getWeekdays } = require("../weekdays");
const Schema = mongoose.Schema;

const irrigationPlansSchema = new Schema({
    name: { type: String, required: true },
    startTime: { type: Date, required: true },
    endTime: {
        type: Date,
        required: true
    },
    userId: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
    repeat: {
        type: String,
        required: true,
        enum: ["Once", "Always", "Custom"]
    },
    days: {
        type: Array,
        validate: {
            validator: (day) => {
                getWeekdays().includes(day);
            },
            msg: "A weekday must be given in input"
        },
        required: () => {
            return this.repeat === "Custom";
        }
    }
});

irrigationPlansSchema.pre("validate", function (next) {
    if (this.startTime >= this.endTime) {
        this.invalidate(
            "startTime",
            "Start date must be less than end date.",
            this.startDate
        );
    }
    next();
});

module.exports = mongoose.model("IrrigationPlan", irrigationPlansSchema);
