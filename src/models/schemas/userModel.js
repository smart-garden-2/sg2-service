const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const UserSchema = new Schema({
    email: { type: String, required: true },
    password: { type: String, required: true },
    authLevel: {
        type: String,
        required: false,
        enum: ["admin", "user"],
        default: "user"
    },
    name: { type: String, required: false },
    surname: { type: String, required: false }
});

module.exports = mongoose.model("User", UserSchema);
