const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const NotificationSchema = new Schema({
    email: { type: String, required: true },
    notification: { type: JSON, required: true }
});

module.exports = mongoose.model("Notification", NotificationSchema);
