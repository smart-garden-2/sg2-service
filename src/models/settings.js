const { check } = require("express-validator");
const settingNames = {
    waterFlow: "waterFlow",
    soilHumidityValue: "soilHumidityThreshold",
    dataGatheringFreq: "dataGatheringFreq"
};

const dataGatheringFreq = {
    EachSecond: "each_second",
    EachMinute: "each_minute",
    EachQuarter: "each_quarter",
    EachHour: "each_hour",
    EachDay: "each_day"
};

const waterFlow = {
    OneQuarter: "ONE_QUARTER",
    Half: "HALF",
    ThreeQuarter: "THREE_QUARTER",
    FullSpeed: "FULL"
};

const minSoilHumidity = 375;
const maxSoilHumidity = 735;

function checkSoilHumidityValue(value) {
    return check(parseInt(value.value)).isInt({
        min: minSoilHumidity,
        max: maxSoilHumidity
    });
}

function checkDataGatheringFreq(value) {
    return Object.values(dataGatheringFreq).includes(value);
}

function checkWaterFlow(value) {
    return Object.values(waterFlow).includes(value);
}

function checkValue(settingName, settingValue) {
    switch (settingName) {
        case "waterFlow":
            if (!checkWaterFlow(settingValue)) {
                return false;
            }
            break;
        case "dataGatheringFreq":
            if (!checkDataGatheringFreq(settingValue)) {
                return false;
            }
            break;
        case "soilHumidityThreshold":
            if (!checkSoilHumidityValue(settingValue)) {
                return false;
            }
            break;
        default:
            return false;
    }
    return true;
}

function checkSettingNames(settingName) {
    return Object.values(settingNames).includes(settingName);
}

module.exports = {
    settingNames,
    checkSettingNames,
    checkValue
};
