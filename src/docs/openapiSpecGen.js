const swaggerJsdoc = require("swagger-jsdoc");
const { expressListenPort } = require("../env");
const swaggerUi = require("swagger-ui-express");

const uiOptions = {
    swaggerOptions: {
        url: `http://localhost:${expressListenPort}/api-docs.json`
    }
};

function getSpec() {
    const options = {
        definition: {
            openapi: "3.0.3",
            info: {
                title: "Smart Garden 2 Service",
                version: "1.0.0",
                description: "RESTful application made with Express",
                license: {
                    name: "Licensed Under the BSD 3-Clause license",
                    url: "https://opensource.org/licenses/BSD-3-Clause"
                }
            },
            servers: [
                {
                    url: `http://localhost:${expressListenPort}`,
                    description: "Development server"
                }
            ]
        },
        apis: ["./src/routes/*Routes.js", "./src/docs/*.yml"]
    };
    return swaggerJsdoc(options);
}

module.exports = {
    getSpec,
    swaggerUi,
    uiOptions
};
