const { verifyToken } = require("./authUtils");

function authenticate(req) {
    const token = req.get("authorization");
    if (!token) {
        return "error: Please provide a token";
    } else return token.split(" ")[1];
}

function authenticateAdmin(req, res, next) {
    const msg = authenticate(req);
    if (msg.startsWith("error")) res.status(401).json({ msg });
    else verifyToken(req, res, next, msg, true);
}

function authenticateUser(req, res, next) {
    const msg = authenticate(req);
    if (msg.startsWith("error")) res.status(401).json({ msg });
    else verifyToken(req, res, next, msg, false);
}

module.exports = {
    authenticateAdmin,
    authenticateUser
};
