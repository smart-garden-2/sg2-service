const tokenSecret = "my-token-secret";
const bcrypt = require("bcrypt");
const { check } = require("express-validator");
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const { cookieDuration } = require("../../env");
const tokenModel = require("../../models/schemas/tokenModel")(mongoose);
const rounds = 10;

function generateToken(user) {
    return jwt.sign({ data: user }, tokenSecret, { expiresIn: cookieDuration });
}

function verifyUserLogin(givenPassword, user, res) {
    bcrypt.compare(givenPassword, user.password, (error, match) => {
        if (error) res.status(500).json(error);
        else if (match) createToken(res, user);
        else res.status(400).json({ error: "passwords do not match" });
    });
}

function verifyToken(req, res, next, token, adminRequested) {
    jwt.verify(token, tokenSecret, (err) => {
        if (err) {
            res.status(401).json({ error: err });
        } else
            tokenModel.findOne({ userToken: token }, (err, doc) => {
                if (err) res.status(404).json({ error: err });
                else if (doc === null)
                    res.status(401).json({ error: "Token not in db" });
                else if (adminRequested && doc.authLevel !== "admin")
                    res.status(403).json({ error: "Admin permit requested" });
                else next();
            });
    });
}

function removeToken(req, res, token) {
    tokenModel.deleteOne({ userToken: token }, (err, result) => {
        if (err) res.status(500).json({ error: err });
        else if (result.deletedCount === 0)
            res.status(500).json({ error: "token not deleted" });
        else
            res.status(200).json({ description: "token successfully removed" });
    });
}

function logOut(req, res) {
    const token = req.get("authorization").split(" ")[1]; // Bearer <token>
    removeToken(req, res, token);
}

function insertToken(authObject, res) {
    authObject.save((err, auth) => {
        if (err) res.send(err);
        res.status(201).json(makeLoginBodyResponse(auth));
    });
}

function makeLoginBodyResponse(auth) {
    return {
        description: "User logged",
        token: auth.userToken,
        email: auth.userEmail,
        authorization: auth.authLevel
    };
}

function createToken(res, user) {
    const authObject = new tokenModel({
        userToken: generateToken(user),
        authLevel: user.authLevel,
        userEmail: user.email
    });
    tokenModel.find({ userEmail: authObject.userEmail }, (err, result) => {
        if (err)
            res.status(400).json({
                error: err
            });
        if (result.length) verifyResult(result[0].userToken, authObject, res);
        else insertToken(authObject, res);
    });
}

function verifyResult(userToken, authObject, res) {
    jwt.verify(userToken, tokenSecret, function (err) {
        if (err) {
            tokenModel.deleteOne({ userToken: userToken }, (err, result) => {
                if (err) res.status(500).json({ error: err });
                else if (result.deletedCount === 0)
                    res.status(500).json({
                        error: "token not deleted "
                    });
                else insertToken(authObject, res);
            });
        } else
            res.status(201).json({
                description: "User logged already",
                token: userToken,
                email: authObject.userEmail,
                authorization: authObject.authLevel
            });
    });
}

function registerNewUser(req, res, User) {
    bcrypt.hash(req.body.password, rounds, (error, hash) => {
        if (error) res.status(500).json(error);
        else {
            const new_user = new User(req.body);
            if (check(new_user.email).isEmail()) {
                new_user.password = hash;
                new_user.save((err, user) => {
                    if (err) res.send(err);
                    res.status(201).json({
                        description:
                            "Operation succeeded. Welcome " + user.email
                    });
                });
            } else
                res.status(404).send({
                    description: "Email not a valid mail !"
                });
        }
    });
}

module.exports = {
    verifyUserLogin,
    registerNewUser,
    logOut,
    verifyToken,
    removeToken
};
