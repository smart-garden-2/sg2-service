const { publishMessage } = require("../../middleware/mqttConn");
const soilHumidityDefaultValue = 555;
const soilHumidityIllegalValue = 1000;

const startAutomaticIrrigation = () => {
    publishMessage(
        "system/soilHumidityThreshold",
        soilHumidityDefaultValue.toString()
    );
};

const stopAutomaticIrrigation = () => {
    publishMessage(
        "system/soilHumidityThreshold",
        soilHumidityIllegalValue.toString()
    );
};

const sendStartIrrigationCommand = (eventEmitter, email) => {
    eventEmitter.emit("startManualIrrigation", email);
    publishMessage("system/waterCmd", "true");
    stopAutomaticIrrigation();
};

const sendStopIrrigationCommand = (eventEmitter, email) => {
    eventEmitter.emit("stopManualIrrigation", email);
    publishMessage("system/waterCmd", "false");
    startAutomaticIrrigation();
};

const startManualIrrigation = (req, res) => {
    sendStartIrrigationCommand(req.app.get("eventEmitter"), req.params.email);
    res.status(202).json({
        status: "Request for starting the manual irrigation sent"
    });
};

const stopManualIrrigation = (req, res) => {
    sendStopIrrigationCommand(req.app.get("eventEmitter"), req.params.email);
    res.status(202).json({
        status: "Request for stopping the manual irrigation sent"
    });
};

module.exports = {
    startManualIrrigation,
    stopManualIrrigation,
    sendStartIrrigationCommand,
    sendStopIrrigationCommand
};
