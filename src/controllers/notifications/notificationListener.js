const {
    irrigationStartNotification,
    irrigationStopNotification
} = require("./notificationLogics");

module.exports = (eventEmitter, socket) => {
    eventEmitter.on("startManualIrrigation", (email) => {
        irrigationStartNotification(socket, email);
    });

    eventEmitter.on("stopManualIrrigation", (email) => {
        irrigationStopNotification(socket, email);
    });
};
