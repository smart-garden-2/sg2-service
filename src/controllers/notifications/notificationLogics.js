const Notifications = require("../../models/schemas/notificationModel");
const { cabin } = require("../../logger");

function sendNotification(json, socket) {
    socket.emit("notification", json);
}

function irrigationStartNotification(socket, email) {
    const json = JSON.stringify({
        status: "Irrigation started",
        date:
            new Date().toJSON().slice(0, 10).replace(/-/g, "/") +
            Date().slice(15, 21)
    });
    addNotificationToDatabase(email, json);
    sendNotification(json, socket);
}

function addNotificationToDatabase(email, json) {
    const body = {
        email: email,
        notification: JSON.parse(json)
    };
    const notification = new Notifications(body);
    notification.save((err) => {
        if (err) cabin.err(err);
    });
}

function irrigationStopNotification(socket, email) {
    const json = JSON.stringify({
        status: "Irrigation stopped",
        date:
            new Date().toJSON().slice(0, 10).replace(/-/g, "/") +
            Date().slice(15, 21)
    });
    addNotificationToDatabase(email, json);
    sendNotification(json, socket);
}

module.exports = {
    irrigationStartNotification,
    irrigationStopNotification
};
