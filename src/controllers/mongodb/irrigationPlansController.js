const IrrigationPlan = require("../../models/schemas/irrigationPlansModel");
const { cabin } = require("../../logger");
const User = require("../../models/schemas/userModel");

const hasDuplicates = (arr) => {
    return arr.some(function (item) {
        return arr.indexOf(item) !== arr.lastIndexOf(item);
    });
};

const getIrrigationPlans = (req, res) => {
    User.findOne({ email: req.params.email })
        .then((user) => {
            IrrigationPlan.find({ userId: user._id }, (err, plans) => {
                if (err) return res.status(404).json({ error: err });
                else res.json(plans);
            });
        })
        .catch((error) => {
            res.status(404).json({ error: error });
        });
};

const addIrrigationPlan = (req, res) => {
    User.findOne({ email: req.body.userId }).then((result) => {
        let irrigationPlanDoc = {
            name: req.body.name,
            userId: result,
            startTime: req.body.startTime,
            endTime: req.body.endTime,
            repeat: req.body.repeat
        };

        if (irrigationPlanDoc.repeat === "Custom" && req.body.days) {
            irrigationPlanDoc.days = req.body.days;
            if (hasDuplicates(req.body.days)) {
                res.status(422).json({ error: "duplicate days found" });
            } else {
                createPlan(irrigationPlanDoc, res, req.app.get("eventEmitter"));
            }
        } else if (irrigationPlanDoc.repeat === "Custom") {
            res.status(422).json({ error: "days must not be empty" });
        } else {
            createPlan(irrigationPlanDoc, res, req.app.get("eventEmitter"));
        }
    });
};

const createPlan = async (doc, res, emitter) => {
    await IrrigationPlan.create(doc, (err, result) => {
        if (err || result == null) {
            cabin.err(err);
            res.status(500).send();
        } else {
            emitter.emit("addIrrigationPlan", result);
            res.status(201).json(result);
        }
    });
};

const deleteIrrigationPlan = (req, res) => {
    const handleDeletion = (err, result) => {
        if (err) return res.status(500).json({ error: err });
        if (result.deletedCount === 0) {
            res.status(404).send({
                description: "Irrigation plan not found"
            });
        } else {
            req.app.get("eventEmitter").emit("deleteIrrigationPlan", result);
            res.json(result);
        }
    };

    IrrigationPlan.findOneAndDelete({ _id: req.params.planId }, handleDeletion);
};

const updateIrrigationPlan = (req, res) => {
    User.findOne({ email: req.body.userId }).then((result) => {
        let irrigationPlanDoc = {
            _id: req.body._id,
            name: req.body.name,
            userId: result,
            startTime: req.body.startTime,
            endTime: req.body.endTime,
            repeat: req.body.repeat
        };

        if (irrigationPlanDoc.repeat === "Custom" && req.body.days) {
            irrigationPlanDoc.days = req.body.days;
            if (hasDuplicates(req.body.days)) {
                res.status(422).json({ error: "duplicate days found" });
            } else {
                updatePlan(irrigationPlanDoc, res, req.app.get("eventEmitter"));
            }
        } else if (irrigationPlanDoc.repeat === "Custom") {
            res.status(422).json({ error: "days must not be empty" });
        } else {
            updatePlan(irrigationPlanDoc, res, req.app.get("eventEmitter"));
        }
    });
};

const updatePlan = (updatedPlan, res, eventEmitter) => {
    const handleUpdate = (err, result) => {
        if (err) res.status(400).json({ error: err });
        else {
            if (result == null) {
                res.status(404).send({
                    description: "Irrigation plan not found"
                });
            } else {
                eventEmitter.emit("updateIrrigationPlan", result);
                res.json(result);
            }
        }
    };

    IrrigationPlan.findOneAndUpdate(
        { _id: updatedPlan._id },
        updatedPlan,
        { new: true },
        handleUpdate
    );
};

module.exports = {
    getIrrigationPlans,
    addIrrigationPlan,
    deleteIrrigationPlan,
    updateIrrigationPlan
};
