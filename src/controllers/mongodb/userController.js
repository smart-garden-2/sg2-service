const {
    registerNewUser,
    verifyUserLogin,
    logOut,
    removeToken
} = require("../authentication/authUtils");
const User = require("../../models/schemas/userModel");

exports.insertNewUser = (req, res) => {
    User.exists({ email: req.body.email }, (err, result) => {
        if (err || result != null) {
            res.status(400).send({
                error: "User already exist!"
            });
        } else {
            registerNewUser(req, res, User);
        }
    });
};

exports.logExistingUser = (req, res) => {
    User.findOne({ email: req.body.email }).then((user) => {
        if (!user)
            res.status(404).json({
                error: "no user with that email found"
            });
        else {
            verifyUserLogin(req.body.password, user, res);
        }
    });
};

exports.logOutExistingUser = (req, res) => {
    logOut(req, res);
};

exports.updateUser = (req, res) => {
    User.findOneAndUpdate(
        { email: req.params.email },
        req.body,
        { new: true },
        (err, user) => {
            if (err) res.send(err);
            else {
                if (user == null) {
                    res.status(404).send({
                        description: "User not found"
                    });
                } else {
                    res.status(200).json(user);
                }
            }
        }
    );
};

exports.deleteUser = (req, res) => {
    User.deleteOne({ email: req.params.email }, (err, result) => {
        if (err) res.send(err);
        else {
            if (result.deletedCount === 0)
                res.status(404).send({
                    description: "User not found"
                });
            else removeToken(req, res, req.get("authorization").split(" ")[1]);
        }
    });
};

exports.getUserInfo = (req, res) => {
    User.findOne({ email: req.params.email }, (err, result) => {
        if (err) res.status(404).send({ error: err });
        else res.status(200).send(result);
    });
};
