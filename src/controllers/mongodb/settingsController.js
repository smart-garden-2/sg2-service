const mongoose = require("mongoose");
const Settings = require("../../models/schemas/settingsModel")(mongoose);
const { updateSettings } = require("./settingsUtils");

exports.showSettings = (req, res) => {
    Settings.find({}, (err, result) => {
        if (err) res.status(500).json({ error: err });
        else res.status(200).json(result);
    });
};

exports.resetToDefault = (req, res) => {
    const callback = (err, docs) => {
        if (err) res.send(err);
        res.status(201).json(docs);
    };
    this.resetValues(callback);
};

exports.resetValues = (callback) => {
    Settings.deleteMany({}, () => {});
    Settings.insertMany(
        [
            new Settings({ name: "waterFlow" }),
            new Settings({ name: "dataGatheringFreq" }),
            new Settings({ name: "soilHumidityThreshold" })
        ],
        callback
    );
};

exports.updateSettings = (req, res) => {
    Settings.findOneAndUpdate(
        { name: req.params.settingName },
        req.body,
        { new: true },
        (err, setting) => updateSettings(err, res, setting)
    );
};
