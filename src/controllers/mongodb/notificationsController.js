const Notifications = require("../../models/schemas/notificationModel");

exports.getNotificationsForUser = (req, res) => {
    Notifications.find({ email: req.params.email }, (err, result) => {
        if (err || result === null) {
            res.status(400).send({
                error: err
            });
        } else {
            res.status(200).json({
                description: result
            });
        }
    });
};

exports.deleteNotification = (req, res) => {
    Notifications.deleteOne(
        { email: req.body.email, notification: req.body.notification },
        (err, result) => {
            if (err)
                res.status(400).json({
                    error: err
                });
            else {
                if (result.deletedCount === 0)
                    res.status(404).json({
                        error: "Notification not found"
                    });
                else
                    res.status(200).json({
                        description: "Notification deleted"
                    });
            }
        }
    );
};
