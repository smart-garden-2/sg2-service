const { settingNames } = require("../../models/settings");
const { publishMessage } = require("../../middleware/mqttConn");

function updateSettings(err, res, setting) {
    if (err || setting === null) res.status(404).json({ error: err });
    else {
        res.status(200).json(setting);
        publishMessage(getMqttChannel(setting.name), setting.value);
    }
}

function getMqttChannel(settingName) {
    let mqttChannel = "system/";
    switch (settingName) {
        case settingNames.waterFlow:
            mqttChannel += settingNames.waterFlow;
            break;
        case settingNames.dataGatheringFreq:
            mqttChannel += settingNames.dataGatheringFreq;
            break;
        case settingNames.soilHumidityValue:
            mqttChannel += settingNames.soilHumidityValue;
            break;
        default:
            mqttChannel = "";
    }
    return mqttChannel;
}

module.exports = {
    updateSettings
};
