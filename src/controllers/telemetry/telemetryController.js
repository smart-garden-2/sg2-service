const { bucket } = require("../../env");
const {
    fluxTelemetryQuery,
    getAllMeasurementsQuery,
    getTelemetryData
} = require("./telemetryStrategy");
const { getWindowFromPeriod } = require("../../models/measurementTimescale");

const getMeasurement = (req, res) => {
    const window = getWindowFromPeriod(req.params.period);
    getTelemetryData(
        fluxTelemetryQuery(
            bucket,
            req.params.period,
            req.params.measurement,
            window
        ),
        res
    );
};

const getAllMeasurements = (req, res) => {
    getTelemetryData(getAllMeasurementsQuery(bucket), res);
};

module.exports = {
    getAllMeasurements,
    getMeasurement
};
