const { cabin } = require("../../logger");

const getDataStream = (topic, socket, client) => {
    client.on("message", function (topic, msg) {
        socket.emit(topic, msg.toString(), (err) => {
            if (err) cabin.error(err);
        });
    });
};

const getTankLevelStream = (socket, client) => {
    getDataStream("system/tankLevel", socket, client);
};

const getTelemetryStream = (socket, client) => {
    getDataStream("system/telemetry", socket, client);
};

const getStateStream = (socket, client) => {
    getDataStream("system/state", socket, client);
};

module.exports = { getTankLevelStream, getTelemetryStream, getStateStream };
