const { flux } = require("@influxdata/influxdb-client");
const { parseTimescale } = require("../../models/measurementTimescale");
const queryApi = require("../../middleware/influxdbConn");

function fluxTelemetryQuery(bucket, start, measurement, window) {
    return flux`from(bucket:${bucket})
                |> range(start: ${parseTimescale(start)})
                |> filter(fn: (r) => r._measurement == ${measurement})
                |> filter(fn: (r) => r._field == "value")
                |> aggregateWindow(every: ${window}, fn: mean)`;
}

function getAllMeasurementsQuery(bucket) {
    return flux`import "influxdata/influxdb/schema" schema.measurements(bucket: ${bucket})`;
}

function getTelemetryData(parametricQuery, res) {
    let buffer = [];
    queryApi.queryRows(parametricQuery, {
        next: (row, tableMeta) => {
            const o = tableMeta.toObject(row);
            buffer.push({ timestamp: o._time, value: o._value });
        },
        error: (error) => {
            res.status(404).json({ error: error });
        },
        complete: () => {
            if (buffer.length === 0) {
                res.status(204).send();
            } else {
                res.json(buffer);
            }
        }
    });
}

module.exports = {
    fluxTelemetryQuery,
    getAllMeasurementsQuery,
    getTelemetryData
};
