const express = require("express");
const { swaggerUi, getSpec, uiOptions } = require("./docs/openapiSpecGen");
const { cabin } = require("./logger");
const influxdbRoutes = require("./routes/telemetryRoutes");
const settingsRoutes = require("./routes/settingsRoutes");
const userRoutes = require("./routes/userRoutes");
const commandsRoutes = require("./routes/commandsRoutes");
const irrigationPlanRoutes = require("./routes/irrigationPlansRoutes");
const notificationRoutes = require("./routes/notificationRoutes");
const { mongodb } = require("./middleware/mongodbConn");
const cors = require("cors");

const app = express();

/*
 * Setup middleware.
 */
//app.use(cabin.middleware);
app.use(cors());
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(null, uiOptions));
app.use(express.json());

/*
 * Connect to mongodb.
 */
mongodb
    .connect()
    .then(cabin.info("Connection to mongodb established successfully"));

/*
 * Setup routes.
 */
app.route("/").get((req, res) => {
    res.send("Hello world");
});

app.route("/api-docs.json").get((req, res) => {
    res.setHeader("Content-Type", "application/json");
    res.send(getSpec());
});

settingsRoutes(app);
userRoutes(app);
influxdbRoutes(app);
commandsRoutes(app);
irrigationPlanRoutes(app);
notificationRoutes(app);

module.exports = app;
