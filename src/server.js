const app = require("./app");
const http = require("http");
const httpServer = http.createServer(app);
const { cabin } = require("./logger");
const { expressListenPort } = require("./env");

const io = require("socket.io")(httpServer, {
    cors: {
        origins: ["http://localhost:8080"]
    }
});

/*
 * Register socket io server
 */
const sessionControl = require("./routes/sessionControl");
sessionControl(io);

/*
 * Start the server.
 */
httpServer.listen(expressListenPort, () => {
    cabin.info(`Server is running at http://localhost:${expressListenPort}`);
});
