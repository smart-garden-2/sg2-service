require("dotenv").config();

const bucket = process.env.INFLUXDB_BUCKET ?? "smart_garden";
const url = process.env.INFLUXDB_URL ?? "http://influxdb:8086";
const token = process.env.INFLUXDB_ADMIN_TOKEN ?? "VEpoAeFohZCwp4Xo5PfXcfopAi";
const org = process.env.INFLUXDB_ORG ?? "home";
const mongodbUsername = process.env.MONGODB_USERNAME ?? "admin";
const mongodbPass = process.env.MONGODB_PASSWORD ?? "smartgarden2";
const mongoHost = process.env.MONGODB_HOST ?? "mongo";
const mongoURI = process.env.MONGO_URI ?? `mongodb://${mongoHost}:27017/`;
const expressListenPort = process.env.EXPRESS_PORT ?? 3000;
const brokerHost = process.env.BROKER_URL ?? "mosquitto";
const cookieDuration = process.env.COOKIE_DURATION ?? "1h";
const dbName = "sg2";

module.exports = {
    expressListenPort,
    url,
    token,
    dbName,
    org,
    bucket,
    brokerHost,
    mongoHost,
    mongodbUsername,
    mongodbPass,
    mongoURI,
    cookieDuration
};
