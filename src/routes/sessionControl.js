const { cabin } = require("../logger");
const {
    getStateStream,
    getTelemetryStream,
    getTankLevelStream
} = require("../controllers/telemetry/telemetryStreamController");

const notificationListener = require("../controllers/notifications/notificationListener");
const irrigationPlanListeners = require("../jobs/irrigationPlanListener");
const { getMqttConnection } = require("../middleware/mqttConn");
const getEventEmitter = require("../middleware/eventEmitter");
const app = require("../app");

function setEventEmitter(socket) {
    const eventEmitter = getEventEmitter();
    app.set("eventEmitter", eventEmitter);
    irrigationPlanListeners(eventEmitter);
    notificationListener(eventEmitter, socket);
}

module.exports = (io) => {
    io.on("connection", (socket) => {
        cabin.info("Connection opened");

        setEventEmitter(socket);

        const mqttClient = getMqttConnection();
        mqttClient.subscribe("system/state");
        getStateStream(socket, mqttClient);

        const telemetryMqttClient = getMqttConnection();
        telemetryMqttClient.subscribe("system/telemetry");
        getTelemetryStream(socket, telemetryMqttClient);

        const tankLevelMqttClient = getMqttConnection();
        tankLevelMqttClient.subscribe("system/tankLevel");
        getTankLevelStream(socket, tankLevelMqttClient);

        socket.on("error", () => cabin.info("An error occurred"));
        socket.on("disconnect", () => {
            mqttClient.end();
            cabin.info("Connection closed");
        });
    });
};
