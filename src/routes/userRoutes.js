const userController = require("../controllers/mongodb/userController");
const {
    authenticateUser
} = require("../controllers/authentication/bearerAuth");

module.exports = (app) => {
    /**
     * @openapi
     * /users:
     *   post:
     *     description: Inserts new user into db.
     *     tags: [Users]
     *     requestBody:
     *       content:
     *         application/json:
     *           schema:
     *             $ref: '#/components/schemas/User'
     *     responses:
     *       201:
     *         $ref: '#/components/responses/TokenDistributionResponse'
     *       400:
     *         $ref: '#/components/responses/BadRequest'
     *       404:
     *         $ref: '#/components/responses/NotFound'
     */
    app.route("/users").post(userController.insertNewUser);

    /**
     * @openapi
     * /users/{email}:
     *   put:
     *     security:
     *       - bearerAuth: []
     *     description: Updates existing user.
     *     tags: [Users]
     *     parameters:
     *       - $ref: '#/parameters/email'
     *     requestBody:
     *       content:
     *         application/json:
     *           schema:
     *             $ref: '#/components/schemas/UpdateUser'
     *     responses:
     *       200:
     *         $ref: '#/components/responses/OperationSuccessResponse'
     *       401:
     *         $ref: '#/components/responses/Unauthorized'
     *       404:
     *         $ref: '#/components/responses/NotFound'
     */
    app.route("/users/:email").put(authenticateUser, userController.updateUser);

    /**
     * @openapi
     * /users/{email}:
     *   delete:
     *     security:
     *       - bearerAuth: []
     *     description: Deletes existing user.
     *     tags: [Users]
     *     parameters:
     *       - $ref: '#/parameters/email'
     *     responses:
     *       200:
     *         $ref: '#/components/responses/OperationSuccessResponse'
     *       401:
     *         $ref: '#/components/responses/Unauthorized'
     *       404:
     *         $ref: '#/components/responses/NotFound'
     *   get:
     *     security:
     *       - bearerAuth: []
     *     description: Gets info about an existing user.
     *     tags: [Users]
     *     parameters:
     *       - $ref: '#/parameters/email'
     *     responses:
     *       200:
     *         $ref: '#/components/responses/OperationSuccessResponse'
     *       401:
     *         $ref: '#/components/responses/Unauthorized'
     *       404:
     *         $ref: '#/components/responses/NotFound'
     */
    app.route("/users/:email")
        .delete(authenticateUser, userController.deleteUser)
        .get(authenticateUser, userController.getUserInfo);

    /**
     * @openapi
     * /users/login:
     *   post:
     *     description: Login user.
     *     tags: [Users]
     *     requestBody:
     *       content:
     *         application/json:
     *           schema:
     *             $ref: '#/components/schemas/UserCredentials'
     *     responses:
     *       201:
     *         $ref: '#/components/responses/OperationSuccessResponse'
     *       400:
     *         $ref: '#/components/responses/BadRequest'
     *       404:
     *         $ref: '#/components/responses/NotFound'
     */
    app.route("/users/login").post(userController.logExistingUser);

    /**
     * @openapi
     * /users/logout:
     *   post:
     *     security:
     *       - bearerAuth: []
     *     description: Logout user.
     *     tags: [Users]
     *     responses:
     *       200:
     *         $ref: '#/components/responses/OperationSuccessResponse'
     *       401:
     *         $ref: '#/components/responses/Unauthorized'
     */
    app.route("/users/logout").post(
        authenticateUser,
        userController.logOutExistingUser
    );
};
