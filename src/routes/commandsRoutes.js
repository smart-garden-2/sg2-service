const mqttCommandsController = require("../controllers/commands/commandsController");
const {
    authenticateUser
} = require("../controllers/authentication/bearerAuth");
module.exports = (app) => {
    /**
     * @openapi
     * /commands/startManualIrrigation/{email}:
     *   put:
     *     security:
     *       - bearerAuth: []
     *     description: Requests the start of the irrigation to the remote station.
     *     tags: [Commands]
     *     parameters:
     *       - $ref: '#/parameters/email'
     *     responses:
     *       202:
     *         $ref: '#/components/responses/RequestSuccessResponse'
     *       401:
     *         $ref: '#/components/responses/Unauthorized'
     */
    app.route("/commands/startManualIrrigation/:email").put([
        authenticateUser,
        mqttCommandsController.startManualIrrigation
    ]);

    /**
     * @openapi
     * /commands/stopManualIrrigation/{email}:
     *   put:
     *     security:
     *       - bearerAuth: []
     *     description: Requests the start of the irrigation to the remote station.
     *     tags: [Commands]
     *     parameters:
     *       - $ref: '#/parameters/email'
     *     responses:
     *       202:
     *         $ref: '#/components/responses/RequestSuccessResponse'
     *       401:
     *         $ref: '#/components/responses/Unauthorized'
     */
    app.route("/commands/stopManualIrrigation/:email").put([
        authenticateUser,
        mqttCommandsController.stopManualIrrigation
    ]);
};
