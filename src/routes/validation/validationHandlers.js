const { validationResult, body } = require("express-validator");

const validateIrrigationPlanRequestBody = [
    body("startTime").isISO8601().toDate(),
    body("endTime").isISO8601().toDate(),
    body("repeat").trim().escape(),
    body("days").optional()
];

const validateReq = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    } else {
        next();
    }
};

module.exports = {
    validateIrrigationPlanRequestBody,
    validateReq
};
