const irrigationPlanController = require("../controllers/mongodb/irrigationPlansController");
const {
    authenticateUser
} = require("../controllers/authentication/bearerAuth");
const {
    validateReq,
    validateIrrigationPlanRequestBody
} = require("./validation/validationHandlers");
const { param } = require("express-validator");

module.exports = (app) => {
    /**
     * @openapi
     * /irrigationPlans/{email}:
     *   get:
     *     security:
     *       - bearerAuth: []
     *     description: Gets all the irrigation plans created by the user.
     *     tags: [Irrigation Plans]
     *     parameters:
     *       - $ref: '#/parameters/email'
     *     responses:
     *       200:
     *         $ref: '#/components/responses/OperationSuccessResponse'
     *       401:
     *         $ref: '#/components/responses/Unauthorized'
     *       404:
     *         $ref: '#/components/responses/NotFound'
     */
    app.route("/irrigationPlans/:email").get([
        authenticateUser,
        validateReq,
        irrigationPlanController.getIrrigationPlans
    ]);

    /**
     * @openapi
     * /irrigationPlans/{planId}:
     *   delete:
     *     security:
     *       - bearerAuth: []
     *     description: Removes the irrigation plan selected by the user.
     *     tags: [Irrigation Plans]
     *     parameters:
     *       - $ref: '#/parameters/planId'
     *     responses:
     *       200:
     *         $ref: '#/components/responses/OperationSuccessResponse'
     *       401:
     *         $ref: '#/components/responses/Unauthorized'
     *       404:
     *         $ref: '#/components/responses/NotFound'
     */
    app.route("/irrigationPlans/:planId").delete(
        [param("planId").trim().escape()],
        [
            authenticateUser,
            validateReq,
            irrigationPlanController.deleteIrrigationPlan
        ]
    );

    /**
     * @openapi
     * /irrigationPlans:
     *   post:
     *     security:
     *       - bearerAuth: []
     *     description: Creates a new irrigation plan.
     *     tags: [Irrigation Plans]
     *     requestBody:
     *       content:
     *         application/json:
     *           schema:
     *             $ref: '#/components/schemas/IrrigationPlan'
     *     responses:
     *       201:
     *         $ref: '#/components/responses/OperationSuccessResponse'
     *       422:
     *         $ref: '#/components/responses/UnprocessableEntity'
     *       401:
     *         $ref: '#/components/responses/Unauthorized'
     *   put:
     *     security:
     *       - bearerAuth: []
     *     description: Updates the irrigation plan.
     *     tags: [Irrigation Plans]
     *     parameters:
     *       - $ref: '#/parameters/planId'
     *     requestBody:
     *       content:
     *         application/json:
     *           schema:
     *             $ref: '#/components/schemas/IrrigationPlan'
     *     responses:
     *       200:
     *         $ref: '#/components/responses/OperationSuccessResponse'
     *       400:
     *         $ref: '#/components/responses/BadRequest'
     *       401:
     *         $ref: '#/components/responses/Unauthorized'
     *       404:
     *         $ref: '#/components/responses/NotFound'
     *       422:
     *         $ref: '#/components/responses/UnprocessableEntity'
     */
    app.route("/irrigationPlans")
        .post(validateIrrigationPlanRequestBody, [
            authenticateUser,
            validateReq,
            irrigationPlanController.addIrrigationPlan
        ])
        .put(validateIrrigationPlanRequestBody, [
            authenticateUser,
            validateReq,
            irrigationPlanController.updateIrrigationPlan
        ]);
};
