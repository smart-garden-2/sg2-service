const influxdbController = require("../controllers/telemetry/telemetryController");
const {
    authenticateUser
} = require("../controllers/authentication/bearerAuth");
const { param } = require("express-validator");
const { validateReq } = require("./validation/validationHandlers");
const { timescales } = require("../models/measurementTimescale");
const { measurements } = require("../models/measurements");

module.exports = (app) => {
    /**
     * @openapi
     * /telemetry/values:
     *   get:
     *     summary: Gets all measures.
     *     security:
     *       - bearerAuth: []
     *     description: A list of all the measurements stored in influxDB.
     *     tags: [Telemetry]
     *     responses:
     *       200:
     *         description: Returns an event stream with a list of the telemetry data gathered by influxDB.
     *         content:
     *           application/json:
     *             schema:
     *               $ref: '#/components/schemas/AllMeasures'
     *       401:
     *         $ref: '#/components/responses/Unauthorized'
     *       404:
     *         $ref: '#/components/responses/NotFound'
     */
    app.route("/telemetry/values").get([
        authenticateUser,
        influxdbController.getAllMeasurements
    ]);

    /**
     * @openapi
     * /telemetry/{period}/{measurement}:
     *   get:
     *     security:
     *       - bearerAuth: []
     *     description: Gets time-series data of the specified measurement from the specified period.
     *     parameters:
     *       - $ref: '#/parameters/measurement'
     *       - $ref: '#/parameters/period'
     *     tags: [Telemetry]
     *     responses:
     *       200:
     *         $ref: '#/components/responses/TelemetryDump'
     *       401:
     *         $ref: '#/components/responses/Unauthorized'
     *       404:
     *         $ref: '#/components/responses/NotFound'
     */
    app.route("/telemetry/:period/:measurement").get(
        [
            param("period").trim().escape().isIn(timescales),
            param("measurement").trim().escape().isIn(measurements)
        ],
        [authenticateUser, validateReq, influxdbController.getMeasurement]
    );
};
