const notificationsController = require("../controllers/mongodb/notificationsController");
const {
    authenticateUser
} = require("../controllers/authentication/bearerAuth");

module.exports = (app) => {
    /**
     * @openapi
     * /notifications/{email}:
     *   get:
     *     security:
     *       - bearerAuth: []
     *     description: Get notifications for some user.
     *     tags: [Notifications]
     *     parameters:
     *       - $ref: '#/parameters/email'
     *     responses:
     *       200:
     *         $ref: '#/components/responses/OperationSuccessResponse'
     *       400:
     *         $ref: '#/components/responses/BadRequest'
     *       401:
     *         $ref: '#/components/responses/Unauthorized'
     */
    app.route("/notifications/:email").get(
        authenticateUser,
        notificationsController.getNotificationsForUser
    );

    /**
     * @openapi
     * /notifications:
     *   delete:
     *     security:
     *       - bearerAuth: []
     *     description: Delete a specified notification.
     *     tags: [Notifications]
     *     requestBody:
     *       content:
     *         application/json:
     *           schema:
     *             $ref: '#/components/schemas/Notifications'
     *     responses:
     *       200:
     *         $ref: '#/components/responses/OperationSuccessResponse'
     *       400:
     *         $ref: '#/components/responses/BadRequest'
     *       401:
     *         $ref: '#/components/responses/Unauthorized'
     *       404:
     *         $ref: '#/components/responses/NotFound'
     */
    app.route("/notifications").delete(
        authenticateUser,
        notificationsController.deleteNotification
    );
};
