const settingsController = require("../controllers/mongodb/settingsController");
const { checkSettingNames, checkValue } = require("../models/settings");
const {
    authenticateAdmin,
    authenticateUser
} = require("../controllers/authentication/bearerAuth");

function checkRequiredParameters(req, res, next) {
    let msg = "";
    let settingName = "";
    let settingValue = "";

    if (!req.params) msg = "No parameters passed";

    if (!req.params.settingName) {
        msg += "No settingName given";
    } else settingName = req.params.settingName;

    if (!checkSettingNames(req.params.settingName))
        msg += "Invalid settingName given";

    if (!req.body.value) {
        msg += "No settingValue given";
    } else settingValue = req.body.value;

    if (!checkValue(settingName, settingValue))
        msg += "Invalid settingValue given";

    if (msg.length !== 0) {
        res.status(400).json({ error: msg }); // Bad Request
    } else {
        return next();
    }
}

function initValues() {
    settingsController.resetValues(() => {});
}

module.exports = (app) => {
    initValues();
    /**
     * @openapi
     * /settings:
     *   get:
     *     security:
     *       - bearerAuth: []
     *     description: Returns all actual settings.
     *     tags: [Settings]
     *     responses:
     *       200:
     *         $ref: '#/components/responses/SettingsResponse'
     *       401:
     *         $ref: '#/components/responses/Unauthorized'
     */
    app.route("/settings").get(
        authenticateUser,
        settingsController.showSettings
    );

    /**
     * @openapi
     * /settings/reset:
     *   get:
     *     security:
     *       - bearerAuth: []
     *     description: Resets settings, to default values.
     *     tags: [Settings]
     *     responses:
     *       201:
     *         $ref: '#/components/responses/OperationSuccessResponse'
     *       401:
     *         $ref: '#/components/responses/Unauthorized'
     *       403:
     *         $ref: '#/components/responses/Forbidden'
     */
    app.route("/settings/reset").get(
        authenticateAdmin,
        settingsController.resetToDefault
    );

    /**
     * @openapi
     * /settings/esp8266/{settingName}:
     *   put:
     *     security:
     *       - bearerAuth: []
     *     description: Updates value for a specified setting in the remote station.
     *     tags: [Settings]
     *     parameters:
     *       - $ref: '#/parameters/settingName'
     *     requestBody:
     *       content:
     *         application/json:
     *           schema:
     *             $ref: '#/components/schemas/SettingUpdate'
     *     responses:
     *       200:
     *         $ref: '#/components/responses/OperationSuccessResponse'
     *       401:
     *         $ref: '#/components/responses/Unauthorized'
     *       403:
     *         $ref: '#/components/responses/Forbidden'
     *       404:
     *         $ref: '#/components/responses/NotFound'
     */
    app.route("/settings/esp8266/:settingName").put(
        authenticateUser,
        checkRequiredParameters,
        settingsController.updateSettings
    );
};
