const agenda = require("../middleware/agendaSingleton");
const { cabin } = require("../logger");
const {
    sendStartIrrigationCommand,
    sendStopIrrigationCommand
} = require("../controllers/commands/commandsController");

const deleteIrrigationJob = async (irrigationPlan) => {
    await agenda.cancel({
        name: `start irrigation ${irrigationPlan._id}`
    });
    await agenda.cancel({
        name: `stop irrigation ${irrigationPlan._id}`
    });
};

const defineJob = (id, eventEmitter, email) => {
    agenda.define(`start irrigation ${id}`, async () => {
        cabin.info("start scheduled irrigation");
        await sendStartIrrigationCommand(eventEmitter, email);
    });
    agenda.define(`stop irrigation ${id}`, async () => {
        cabin.info("stop scheduled irrigation");
        await sendStopIrrigationCommand(eventEmitter, email);
    });
};

const findUserEmail = (irrigationPlan, eventEmitter) => {
    const User = require("../models/schemas/userModel");
    User.findOne({ _id: irrigationPlan.userId }).then((res) => {
        defineJob(irrigationPlan._id, eventEmitter, res.email);
    });
};

const schedulePlanOnce = async (irrigationPlan, eventEmitter) => {
    const startTime = irrigationPlan.startTime;
    const endTime = irrigationPlan.endTime;

    findUserEmail(irrigationPlan, eventEmitter);

    await (async function () {
        await agenda.start();
        await agenda.schedule(
            new Date(startTime),
            `start irrigation ${irrigationPlan._id}`,
            {}
        );
        await agenda.schedule(
            new Date(endTime),
            `stop irrigation ${irrigationPlan._id}`,
            {}
        );
    })();
};

const schedulePlanEveryDay = async (irrigationPlan, eventEmitter) => {
    const startTime = irrigationPlan.startTime;
    const endTime = irrigationPlan.endTime;
    findUserEmail(irrigationPlan, eventEmitter);

    await (async function () {
        await agenda.start();
        await agenda.every(
            `${startTime.getMinutes()} ${startTime.getHours()} * * *`,
            `start irrigation ${irrigationPlan._id}`
        );
        await agenda.every(
            `${endTime.getMinutes()} ${endTime.getHours()} * * *`,
            `stop irrigation ${irrigationPlan._id}`
        );
    })();
};

const scheduleCustomPlan = async (irrigationPlan, eventEmitter) => {
    const startTime = irrigationPlan.startTime;
    const endTime = irrigationPlan.endTime;
    findUserEmail(irrigationPlan, eventEmitter);

    await (async function () {
        await agenda.start();
        await agenda.every(
            `${startTime.getMinutes()} ${startTime.getHours()} * * ${startTime.getDay()}`,
            `start irrigation ${irrigationPlan._id}`
        );
        await agenda.every(
            `${endTime.getMinutes()} ${endTime.getHours()} * * ${endTime.getDay()}`,
            `stop irrigation ${irrigationPlan._id}`
        );
    })();
};

module.exports = {
    deleteIrrigationJob,
    schedulePlanOnce,
    schedulePlanEveryDay,
    scheduleCustomPlan
};
