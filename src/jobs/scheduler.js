const {
    deleteIrrigationJob,
    schedulePlanEveryDay,
    schedulePlanOnce,
    scheduleCustomPlan
} = require("./schedulerStrategy");

const schedule = {
    setIrrigationPlan: async (plan, eventEmitter) => {
        if (plan.repeat === "Once") await schedulePlanOnce(plan, eventEmitter);
        if (plan.repeat === "Always")
            await schedulePlanEveryDay(plan, eventEmitter);
        if (plan.repeat === "Custom")
            await scheduleCustomPlan(plan, eventEmitter);
    },
    unsetIrrigationPlan: async (plan) => {
        await deleteIrrigationJob(plan);
    }
};

module.exports = { schedule };
