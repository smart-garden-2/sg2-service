const { schedule } = require("./scheduler");

module.exports = (eventEmitter) => {
    eventEmitter.on("addIrrigationPlan", async (irrigationPlan) => {
        await schedule.setIrrigationPlan(irrigationPlan, eventEmitter);
    });

    eventEmitter.on("deleteIrrigationPlan", async (irrigationPlan) => {
        await schedule.unsetIrrigationPlan(irrigationPlan);
    });

    eventEmitter.on("updateIrrigationPlan", async (irrigationPlan) => {
        await schedule.unsetIrrigationPlan(irrigationPlan);
        await schedule.setIrrigationPlan(irrigationPlan, eventEmitter);
    });
};
