const EventEmitter = require("events");

function getEventEmitter() {
    class MyEmitter extends EventEmitter {}
    return new MyEmitter();
}

module.exports = getEventEmitter;
