const { InfluxDB } = require("@influxdata/influxdb-client");
const { url, token, org } = require("../env");

const queryApi = new InfluxDB({ url, token }).getQueryApi(org);

module.exports = queryApi;
