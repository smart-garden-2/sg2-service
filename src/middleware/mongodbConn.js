const { dbName, mongodbPass, mongodbUsername, mongoURI } = require("../env");
const mongoose = require("mongoose");
const { cabin } = require("../logger");

const mongodb = {
    connect: async () => {
        await mongoose.connect(
            mongoURI + dbName,
            {
                authSource: "admin",
                user: mongodbUsername,
                pass: mongodbPass
            },
            (err) => {
                if (err) return cabin.err(err);
            }
        );
    },
    close: async () => {
        await mongoose.connection.dropDatabase();
        await mongoose.connection.close();
    },
    clear: async () => {
        const collections = mongoose.connection.collections;
        for (const key in collections) {
            await collections[key].deleteMany({});
        }
    }
};

module.exports = {
    mongodb
};
