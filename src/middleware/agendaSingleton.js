const Agenda = require("agenda");
const { mongodbUsername, mongodbPass, mongoHost } = require("../env");
const { cabin } = require("../logger");

const agenda = new Agenda({
    db: {
        address: `mongodb://${mongodbUsername}:${mongodbPass}@${mongoHost}:27017/agenda`,
        collection: `agenda`,
        options: { useNewUrlParser: true, authSource: "admin" }
    },
    processEvery: "5 seconds"
});

agenda
    .on("ready", () => cabin.info("Agenda is running!"))
    .on("error", () => cabin.info("Agenda connection error!"));

async function graceful() {
    await agenda.stop();
    process.exit(0);
}

process.on("SIGTERM", graceful);
process.on("SIGINT", graceful);

module.exports = agenda;
