const mqtt = require("mqtt");
const { cabin } = require("../logger");
const { brokerHost } = require("../env");

function getMqttConnection() {
    const host = brokerHost;
    const port = "1883";
    const clientId = `mqtt_${Math.random().toString(16).slice(3)}`;
    const connectUrl = `mqtt://${host}:${port}`;
    return mqtt.connect(connectUrl, {
        clientId,
        clean: true,
        connectTimeout: 4000,
        reconnectPeriod: 1000
    });
}

function publishMessage(topic, message) {
    const client = getMqttConnection();
    client.on("connect", () => {
        client.publish(topic, message, { qos: 0, retain: false }, (err) => {
            if (err) {
                cabin.error(err);
            }
        });
        client.end();
    });
}

module.exports = {
    publishMessage,
    getMqttConnection
};
