const Cabin = require("cabin");
const Signale = require("signale/signale");
const cabin = new Cabin({
    axe: {
        logger: new Signale()
    }
});
module.exports = { cabin };
